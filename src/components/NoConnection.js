import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';

const NoConnection = () => {
  return (
    <View style={styles.ConnectionContainer}>
      <Icon name="wifi-off" size={100} color={'#4267CD'}></Icon>
      <Text style={styles.ConnectionText}>No Connection</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  ConnectionContainer: {
    alignItems: 'center',
  },
  ConnectionText: {
    fontSize: 20,
    marginTop: 15,
    fontWeight: 'bold',
    color: '#404C6D',
  },
});

export default NoConnection;
