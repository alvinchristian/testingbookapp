import React from "react";
import { Provider } from "react-redux";
import { store } from "../store/Index";
import { NavigationContainer } from "@react-navigation/native";

export default function TestingContainer(component) {
    return (
        <Provider store={store}>
            <NavigationContainer>{component}</NavigationContainer>
        </Provider>
    );
}
