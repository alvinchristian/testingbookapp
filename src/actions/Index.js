import axios from 'axios';
import NetInfo from '@react-native-community/netinfo';
import {
  GET_USER_DATA,
  CLEAR_USER_DATA,
  REGISTER_CHECKER,
  CLEAR_REGISTER_STATUS,
  GET_BOOKS,
  GET_RECOMMENDED_BOOKS,
  GET_BOOK_DETAIL,
  CLEAR_BOOKS_DATA,
  ALERT_SHOW,
  ALERT_HIDE,
  ALERT_DATA,
  CONNECTED,
  NOT_CONNECTED,
} from '../types/Index';

const BaseUrl = 'http://code.aldipee.com/api/v1';
const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-.]+\.[a-zA-Z]{2,6}$/;
const passPattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

export const saveUserData = data => ({
  type: GET_USER_DATA,
  payload: data,
});

export const registerChecker = () => ({
  type: REGISTER_CHECKER,
});

export const clearRegisterStatus = () => ({
  type: CLEAR_REGISTER_STATUS,
});

export const clearUserData = () => ({
  type: CLEAR_USER_DATA,
});

export const saveBooks = data => ({
  type: GET_BOOKS,
  payload: data,
});

export const getRecBooks = data => ({
  type: GET_RECOMMENDED_BOOKS,
  payload: data,
});

export const getBookDetail = data => ({
  type: GET_BOOK_DETAIL,
  payload: data,
});

export const clearBooksData = () => ({
  type: CLEAR_BOOKS_DATA,
});

export const alertShow = () => ({
  type: ALERT_SHOW,
});

export const alertHide = () => ({
  type: ALERT_HIDE,
});

export const alertData = (icon, color, title, message) => ({
  type: ALERT_DATA,
  payload: {icon, color, title, message},
});

export const connected = () => ({
  type: CONNECTED,
});

export const notConnected = () => ({
  type: NOT_CONNECTED,
});

export const getUser = data => {
  return async dispatch => {
    const {email, password} = data;
    try {
      if (email == '' || password == '') {
        dispatch(
          alertData(
            'alert-circle',
            'red',
            'ERROR',
            'Please Input Email and Password!',
          ),
        );
        dispatch(alertShow());
      } else if (!emailPattern.test(email) && !passPattern.test(password)) {
        dispatch(
          alertData(
            'alert-circle',
            'red',
            'ERROR',
            'Invalid Email and Password! Email must contain "@", and domain name! - Password Minimum Eight Characters, at Least One Letter and One Number!',
          ),
        );
        dispatch(alertShow());
      } else if (!emailPattern.test(email) && passPattern.test(password)) {
        dispatch(
          alertData(
            'alert-circle',
            'red',
            'ERROR',
            'Invalid Email! Email must contain "@", and domain name!',
          ),
        );
        dispatch(alertShow());
      } else if (emailPattern.test(email) && !passPattern.test(password)) {
        dispatch(
          alertData(
            'alert-circle',
            'red',
            'ERROR',
            'Invalid Password! Password Minimum Eight Characters, at Least One Letter and One Number!',
          ),
        );
        dispatch(alertShow());
      } else if (emailPattern.test(email) && passPattern.test(password)) {
        const resGetUser = await axios.post(BaseUrl + '/auth/login', data);
        if (resGetUser.data) {
          dispatch(saveUserData(resGetUser.data));
        }
      }
    } catch (err) {
      if (err.response.status === 401) {
        dispatch(
          alertData(
            'alert-circle',
            'red',
            'ERROR',
            'Incorrect Email and Password',
          ),
        );
        dispatch(alertShow());
      }
    }
  };
};

export const signUp = data => {
  return async dispatch => {
    const {name, email, password} = data;
    try {
      if (name == '' || email == '' || password == '') {
        dispatch(
          alertData(
            'alert-circle',
            'red',
            'ERROR',
            'Please Input Fullname, Email and Password!',
          ),
        );
        dispatch(alertShow());
      } else if (!emailPattern.test(email) && !passPattern.test(password)) {
        dispatch(
          alertData(
            'alert-circle',
            'red',
            'ERROR',
            'Invalid Email and Password! Email must contain "@", and domain name! - Password Minimum Eight Characters, at Least One Letter and One Number!',
          ),
        );
        dispatch(alertShow());
      } else if (!emailPattern.test(email) && passPattern.test(password)) {
        dispatch(
          alertData(
            'alert-circle',
            'red',
            'ERROR',
            'Invalid Email! Email must contain "@", and domain name!',
          ),
        );
        dispatch(alertShow());
      } else if (emailPattern.test(email) && !passPattern.test(password)) {
        dispatch(
          alertData(
            'alert-circle',
            'red',
            'ERROR',
            'Invalid Password! Password Minimum Eight Characters, at Least One Letter and One Number!',
          ),
        );
        dispatch(alertShow());
      } else if (emailPattern.test(email) && passPattern.test(password)) {
        const resSignUp = await axios.post(BaseUrl + '/auth/register', data);
        if (resSignUp.data) {
          dispatch(registerChecker());
        }
      }
    } catch (err) {
      if (err.response.status === 400) {
        dispatch(
          alertData('alert-circle', 'red', 'ERROR', 'Email already taken!'),
        );
        dispatch(alertShow());
      }
    }
  };
};

export const getBooks = userToken => {
  return async dispatch => {
    try {
      const resGetBooks = await axios.get(BaseUrl + '/books', {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      });
      if (resGetBooks.data) {
        dispatch(saveBooks(resGetBooks.data.results));
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const recBooks = userToken => {
  return async dispatch => {
    try {
      const resRecBooks = await axios.get(BaseUrl + '/books', {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      });
      const recBooksData = resRecBooks.data.results
        .sort((a, b) => b.average_rating - a.average_rating)
        .slice(0, 6);
      if (recBooksData) {
        dispatch(getRecBooks(recBooksData));
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const getDetailBook = (userToken, bookId) => {
  return async dispatch => {
    try {
      const resGetDetailBook = await axios.get(`${BaseUrl}/books/${bookId}`, {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      });
      if (resGetDetailBook.data) {
        dispatch(getBookDetail(resGetDetailBook.data));
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const connectionChecker = () => {
  return async dispatch => {
    try {
      NetInfo.addEventListener(state => {
        if (state.isConnected) {
          dispatch(connected());
        } else {
          dispatch(notConnected());
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const rupiah = number => {
  let reverse = number.toString().split('').reverse().join(''),
    thousand = reverse.match(/\d{1,3}/g);
  thousand = thousand.join('.').split('').reverse().join('');
  return thousand;
};
