import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {
  getUser,
  alertData,
  alertShow,
  connectionChecker,
} from '../actions/Index';
import AlertView from '../components/AlertView';
import Input from '../components/Input';
import Button from '../components/Button';
import NoConnection from '../components/NoConnection';

const Login = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isSecure, setIsSecure] = useState(true);

  const UserData = useSelector(state => state.authData.userData);
  const connection = useSelector(state => state.utilsData.connectionStatus);

  useEffect(() => {
    dispatch(connectionChecker());
    if (isFocused) {
      if (UserData != null) {
        if (UserData.user.isEmailVerified && UserData.tokens.access.token) {
          // dispatch(
          //   alertData('check-circle', 'green', 'SUCCESS', 'Login Successful!'),
          // );
          // dispatch(alertShow());
          navigation.navigate('Home');
        } else {
          dispatch(
            alertData(
              'alert-circle',
              'red',
              'ERROR',
              "Please Verify Your Email! If there isn't Email in Inbox, Please Check Spam!",
            ),
          );
          dispatch(alertShow());
        }
      }
    }
  }, [connection, isFocused, UserData]);

  return (
    <KeyboardAvoidingView
      testID="loginScreen"
      style={styles.Container}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <StatusBar backgroundColor={'#F0F4FF'} barStyle="dark-content" />
      {!connection ? (
        <NoConnection />
      ) : (
        <View style={styles.MainContainer}>
          <Image style={styles.Logo} source={require('../assets/logo.png')} />
          <View style={styles.InputField}>
            <Input
              testID={'emailInputL'}
              placheholder="Email"
              onChangeText={value => setEmail(value)}
            />
            <Input
              testID={'passwordInputL'}
              placheholder="Password"
              secureTextEntry={isSecure}
              onChangeText={value => setPassword(value)}
            />
            <Button
              testID={'loginButton'}
              text={'LOGIN'}
              onPress={() => dispatch(getUser({email, password}))}
            />
          </View>
          <View style={styles.Register}>
            <Text style={styles.Ask}>Don't have an account? </Text>
            <TouchableOpacity
              testID="registerDirect"
              onPress={() => navigation.navigate('Register')}>
              <Text style={styles.Answer}>Register</Text>
            </TouchableOpacity>
          </View>
          <AlertView />
        </View>
      )}
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F0F4FF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  MainContainer: {
    paddingTop: '20%',
    alignItems: 'center',
  },
  Logo: {
    width: 180,
    height: 180,
    marginBottom: 40,
  },
  InputField: {
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 60,
  },
  Register: {
    alignItems: 'center',
  },
  Ask: {
    marginBottom: 6,
    fontSize: 12,
    color: '#404C6D',
  },
  Answer: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#404C6D',
  },
});

export default Login;
