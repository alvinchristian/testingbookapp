import {View, Text, StyleSheet, StatusBar} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import Button from '../components/Button';
import {useNavigation} from '@react-navigation/native';

const SuccessRegister = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.Container} testID="successRegisterScreen">
      <StatusBar backgroundColor={'#F0F4FF'} barStyle="dark-content" />
      <Text style={styles.Title}>Registration Completed!</Text>
      <View style={styles.Center}>
        <Icon style={styles.Icon} name="check" size={50} color="#4267CD" />
        <Text style={styles.CenterText}>
          We Send Email Verification to Your Email
        </Text>
      </View>
      <Button
        testID={'backToLogin'}
        text={'Back To Login'}
        onPress={() => navigation.navigate('Login')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F0F4FF',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: '20%',
  },
  Center: {
    alignItems: 'center',
  },
  Icon: {
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 50,
    marginBottom: 20,
  },
  CenterText: {
    color: '#404C6D',
    fontSize: 14,
  },
  Title: {
    color: '#404C6D',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default SuccessRegister;
