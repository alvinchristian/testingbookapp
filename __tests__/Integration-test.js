import React from "react";
import MockAdapter from "axios-mock-adapter";
import axios from "axios";
import { loginData, registerData, booksData, bookDetailData } from "../src/utils/DummyData";

describe("Api Testing!", () => {
    let mock = new MockAdapter(axios);
    const BaseUrl = "http://code.aldipee.com/api/v1";

    test("Login Api!", async () => {
        let loginBody = {
            email: "alvintherry20@gmail.com",
            password: "alvinbinar1",
        };

        mock.onPost("http://code.aldipee.com/api/v1/auth/login").reply(200, loginData);
        let res = await axios.post(BaseUrl + "/auth/login", loginBody);
        expect(res.data).toEqual(loginData);
        expect(res.status).toEqual(200);
    });

    test("Register Api!", async () => {
        let registerBody = {
            name: "integrationtest",
            email: "integrationtest@gmail.com",
            password: "integrationtest1",
        };

        mock.onPost("http://code.aldipee.com/api/v1/auth/register").reply(200, registerData);
        let res = await axios.post(BaseUrl + "/auth/register", registerBody);
        expect(res.data).toEqual(registerData);
        expect(res.status).toEqual(200);
    });

    test("Books Api!", async () => {
        let token =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjRkMzgyNWJiZTlkZDdmYjRmODgxMGMiLCJpYXQiOjE2NTA2NDc0NzEsImV4cCI6MTY1MDY0OTI3MSwidHlwZSI6ImFjY2VzcyJ9.NB067lMhyljMKLsEHUTH93PFP6SlOud-6nboIoj90xs";

        mock.onGet("http://code.aldipee.com/api/v1/books").reply(200, booksData);
        let res = await axios.get(BaseUrl + "/books", {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        expect(res.data).toEqual(booksData);
        expect(res.status).toEqual(200);
    });

    test("Detail Book Api!", async () => {
        let token =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjRkMzgyNWJiZTlkZDdmYjRmODgxMGMiLCJpYXQiOjE2NTA2NDc0NzEsImV4cCI6MTY1MDY0OTI3MSwidHlwZSI6ImFjY2VzcyJ9.NB067lMhyljMKLsEHUTH93PFP6SlOud-6nboIoj90xs";

        mock.onGet("http://code.aldipee.com/api/v1/books/6231453513c01e6f8b566ece").reply(
            200,
            bookDetailData
        );
        let res = await axios.get(BaseUrl + "/books/6231453513c01e6f8b566ece", {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        expect(res.data).toEqual(bookDetailData);
        expect(res.status).toEqual(200);
    });
});
