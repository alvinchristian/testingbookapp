import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import Login from '../src/containers/Login';
import Register from '../src/containers/Register';
import TestingContainer from '../src/utils/TestingContainer';

test('Login Button', () => {
  const {queryByTestId} = render(TestingContainer(<Login />));
  const emailInput = queryByTestId('emailInputL');
  const passwordInput = queryByTestId('passwordInputL');
  const loginButton = queryByTestId('loginButton');

  fireEvent.changeText(emailInput, 'alvintherry20@gmail.com');
  fireEvent.changeText(passwordInput, 'alvinbinar1');
  fireEvent.press(loginButton);
});

test('Register Button', () => {
  const {queryByTestId} = render(TestingContainer(<Register />));
  const nameInput = queryByTestId('nameInputR');
  const emailInput = queryByTestId('emailInputR');
  const passwordInput = queryByTestId('passwordInputR');
  const registerButton = queryByTestId('registerButton');

  fireEvent.changeText(nameInput, 'functionstate');
  fireEvent.changeText(emailInput, 'functionstate@gmail.com');
  fireEvent.changeText(passwordInput, 'functionstate1');
  fireEvent.press(registerButton);
});
