import React from 'react';
import Splash from '../src/containers/Splash';
import Login from '../src/containers/Login';
import Register from '../src/containers/Register';
import Home from '../src/containers/Home';
import TestingContainer from '../src/utils/TestingContainer';
import {render} from '@testing-library/react-native';

describe('Snapshot Testing!', () => {
  test('Splash Snapshot!', () => {
    const {toJSON} = render(TestingContainer(<Splash />));
    expect(toJSON()).toMatchSnapshot();
  });
  test('Login Snapshot!', () => {
    const {toJSON} = render(TestingContainer(<Login />));
    expect(toJSON()).toMatchSnapshot();
  });
  test('Register Snapshot!', () => {
    const {toJSON} = render(TestingContainer(<Register />));
    expect(toJSON()).toMatchSnapshot();
  });
  test('Home Snapshot!', () => {
    const {toJSON} = render(TestingContainer(<Home />));
    expect(toJSON()).toMatchSnapshot();
  });
});
