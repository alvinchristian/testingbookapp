describe('Register Test!', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('should have splash screen', async () => {
    await expect(element(by.id('splashScreen'))).toBeVisible();
    await new Promise(resolve => setTimeout(resolve, 2000));
  });

  it('should have login screen', async () => {
    await expect(element(by.id('loginScreen'))).toBeVisible();
  });

  it('should have register direction', async () => {
    await element(by.id('registerDirect')).tap();
  });

  it('should have register screen', async () => {
    await expect(element(by.id('registerScreen'))).toBeVisible();
  });

  it('should fill register form', async () => {
    await element(by.id('nameInputR')).typeText('e2etesting23');
    await element(by.id('emailInputR')).typeText('e2etesting23@gmail.com');
    await element(by.id('passwordInputR')).typeText('e2etesting23');
  });

  it('should have register button', async () => {
    await element(by.id('registerButton')).tap();
  });
});
