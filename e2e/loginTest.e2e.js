describe('Login Test!', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('should have splash screen', async () => {
    await expect(element(by.id('splashScreen'))).toBeVisible();
    await new Promise(resolve => setTimeout(resolve, 2000));
  });

  it('should have login screen', async () => {
    await expect(element(by.id('loginScreen'))).toBeVisible();
  });

  it('should fill login form', async () => {
    await element(by.id('emailInputL')).typeText('alvintherry20@gmail.com');
    await element(by.id('passwordInputL')).typeText('alvinbinar1');
  });

  it('should have login button', async () => {
    await element(by.id('loginButton')).tap();
  });
});
